let vds = {};

function captureCamera(callback) {
    navigator.mediaDevices.getUserMedia({ audio: false, video: true }).then(function(camera) {
        callback(camera);
    }).catch(function(error) {
        // alert('Unable to capture your camera. Please check console logs.');
        console.error(error);
    });
}

function appendVideo(main) {
    let video = document.createElement("video");
    video.setAttribute("id", "video" + currID);
    video.style.opacity = opacity;
    if(main)
        video.style.opacity = mainOpacity;
    video.autoplay = true;
    video.style.zIndex = currID;
    videos.appendChild(video);
    let key = "video" + currID;
    vds[key] = video;
    return vds[key];
}

function startCapture() {
    let video = appendVideo(true);
    videos.style.left = "calc(50% - " + video.offsetWidth/3 + "px)";
    document.getElementById("tr-video").style.width = video.offsetWidth/3*2-4 + "px";
    captureCamera(function(camera) {
        video.muted = true;
        video.volume = 0;
        video.srcObject = camera;
        recorder = RecordRTC(camera, {
            type: 'video'
        });
        recorder.startRecording();
        recorder.camera = camera;
    });
}

function stopRecordingCallback() {
    let url = URL.createObjectURL(recorder.getBlob());
    let video = appendVideo();
    video.setAttribute("src", url);
    video.loop = repeat;
    if(currID > stopAt && currID < videosCount) {
        let vidKey = "video"+(currID - stopAt);
        vds[vidKey].style.opacity = "0";
        vds[vidKey].remove();
        delete vds[vidKey];
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

